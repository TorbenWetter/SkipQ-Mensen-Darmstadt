package de.yoshiswelt.skipq_mensen;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Input extends Thread {

    private final Socket socket;

    Input(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            // Erste Information (am Anfang) über die Anfrage wird in die Konsole geprintet
            String address = String.valueOf(socket.getInetAddress());
            final String time = Main.sdf.format(Calendar.getInstance().getTime());
            System.out.println(
                    time + "Anfrage von " + address.substring(1, address.length()) + ":" + socket.getPort() + "..");

            final DataInputStream dis = new DataInputStream(socket.getInputStream());

            // Der Client sendet den Namen der Mensa
            final String mensaName = dis.readUTF();
            // Durch toUrlString() wird der mensaName, welcher noch Umlaute enthält, zu einem "gültigen" Url-Text umgewandelt
            final String link = "http://studierendenwerkdarmstadt.de/hochschulgastronomie/speisekarten/" + toUrlString(mensaName);

            // Der Client sendet das Datum der gewünschten Speisekarte; TODO: nur Tage aus der aktuellen Woche zulassen
            final Date date = getDateFromString(dis.readUTF());
            // Der Index des angegebenen Tages in der Woche (Montag = 0, Sonntag = 6) wird ermittelt,
            // damit der richtige Teil aus der Website ausgewählt werden kann
            final Integer weekdayIndex = getWeekdayIndex(date);

            // Website wird ausgelesen und in String content gespeichert
            final BufferedReader br = getBufferedReader(link);
            String inputLine;
            final StringBuilder sb = new StringBuilder();
            while ((inputLine = br.readLine()) != null)
                sb.append("\n").append(inputLine);
            br.close();
            final String content = sb.toString();

            final String startOfDay = "<div class=\"fmc-head\">";
            final String endOfDay = "\n</section><section class=\"fmc-day \">";
            // Teil aus der Website für den angegebenen Tag wird in String day gespeichert
            final String day = startOfDay + content.split(startOfDay)[weekdayIndex + 1].replace(endOfDay, "");

            final String startOfFood = "<span class=\"fmc-item-title\">";
            final String endOfFood = "</span>";
            // Es werden grob die einzelnen Produkte getrennt in ein String[] gespeichert
            final String[] theRawFood = day.split(startOfFood);
            // Erstes Element aus dem Array (Inhalt vor den Produkten -> Datum, etc.) wird entfernt
            final String[] rawFood = Arrays.copyOfRange(theRawFood, 1, theRawFood.length);
            // Die einzelnen Produkte werden zurechtgeschnitten, damit nur noch relevante Informationen enthalten bleiben
            for (Integer i = 0; i < rawFood.length; i++)
                rawFood[i] = startOfFood + rawFood[i].substring(0, nthIndexOf(rawFood[i], endOfFood, 3) + endOfFood.length());
            // Zeilenumbrüche und Tabs werden entfernt und die nun sorgfältig ausgelesenen Produkte werden in das String[] food gespeichert
            final String[] food = new String[rawFood.length];
            for (Integer i = 0; i < rawFood.length; i++)
                food[i] = rawFood[i].replace("\n", "").replace("\t", "");

            final DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            // Die Anzahl der Produkte wird an den Client gesendet
            dos.writeInt(food.length);
            // Alle Produkte werden durchgegangen, die Informationen (Name, Ort, Preis) werden aus ihnen ausgelesen und an den Client gesendet
            for (String theFood : food) {
                final String endOfInfo = "</span>";

                final String startOfTitle = "<span class=\"fmc-item-title\">";
                final String title = theFood.substring(theFood.indexOf(startOfTitle) + startOfTitle.length(), theFood.indexOf(endOfInfo)).trim();

                final String startOfLocation = "<span class=\"fmc-item-location\">";
                final String location = theFood.substring(theFood.indexOf(startOfLocation) + startOfLocation.length(), nthIndexOf(theFood, endOfInfo, 2)).trim();

                final String startOfPrice = "<span class=\"fmc-item-price\">";
                final String price = theFood.substring(theFood.indexOf(startOfPrice) + startOfPrice.length(), nthIndexOf(theFood, endOfInfo, 3)).replace(" ", "");

                // Split-String ist in dem Fall "/\\/", da er nicht im Produktnamen, etc. vorkommen darf; TODO: evtl. einen "sinnvolleren" benutzen
                final String splitString = "/\\\\/";
                dos.writeUTF(title + splitString + location + splitString + price);
            }

            socket.close();

            // Zweite Information (am Ende) über die Anfrage wird in die Konsole geprintet
            final String time1 = Main.sdf.format(Calendar.getInstance().getTime());
            System.out.println(time1 + "..Speiseplan der Mensa " + mensaName + " vom " + Main.sdf2.format(date) + " gesendet!");
            System.out.println(time1 + "----------------------------------");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Umlaute, usw. werden ersetzt (ß -> ss, ä -> ae, ...)
    private String toUrlString(String mensaName) {
        return mensaName.toLowerCase().replace("ß", "ss").replace("ä", "ae").replace("ö", "oe").replace("ü", "ue");
    }

    // Erweiterung der String.indexOf() Methode; n'tes Vorkommen kann gefunden werden
    private Integer nthIndexOf(String string, String regex, Integer n) {
        Integer position = string.indexOf(regex);
        while (--n > 0 && position != -1)
            position = string.indexOf(regex, position + 1);
        return position;
    }

    // Der Index des angegebenen Tages in der Woche (Montag = 0, Sonntag = 6) wird ermittelt
    private Integer getWeekdayIndex(Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final Integer weekdayIndex = calendar.get(Calendar.DAY_OF_WEEK);
        return weekdayIndex == 1 ? 6 : weekdayIndex - 2;
    }

    // Ein Datum als String wird in ein deutsches Date-Objekt konvertiert
    private Date getDateFromString(String date) {
        try {
            return new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY).parse(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    // Es wird eine Verbindung zur angegebenen Website hergestellt, welche dann als BufferedReader zum Auslesen bereit gemacht wird
    private BufferedReader getBufferedReader(String link) throws IOException {
        final URL url = new URL(link);
        final HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

        return new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));
    }
}