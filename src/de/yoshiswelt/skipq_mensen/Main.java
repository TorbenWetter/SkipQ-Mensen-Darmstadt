package de.yoshiswelt.skipq_mensen;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static List<String> ips = new ArrayList<>();

    static final SimpleDateFormat sdf = new SimpleDateFormat("<dd.MM.yyyy | HH:mm:ss> ");
    static final SimpleDateFormat sdf2 = new SimpleDateFormat("dd.MM.yyyy");

    public static void main(String[] args) {
        System.out.println("Server wurde gestartet! (Port: 12567)");
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(12567);

            while (true) {
                final Socket socket = serverSocket.accept();
                final String ip = String.valueOf(socket.getInetAddress());
                if (!ips.contains(ip)) {
                    ips.add(ip);
                    new Thread(() -> {
                        try {
                            Thread.sleep(1000);
                            ips.remove(ip);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }).start();
                    final Input input = new Input(socket);
                    input.start();
                } else
                    socket.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (serverSocket != null)
                    serverSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}